@extends('backoffice.backoffice')

@section('title', 'Laravel Test | '.$Type )

@section('extra-css')

    <!-- iCheck -->
    <link href="/backoffice/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="/backoffice/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/backoffice/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/backoffice/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/backoffice/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/backoffice/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

@endsection

@section('content')
        <!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <ol class="breadcrumb">
                    <li><a href="{{ route('listes.index') }}">Listes</a></li>
                </ol>

            </div>
            <div class="title_right">
                <div class="pull-right">
                    <a href="{{ route('listitem.create', $id) }}" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        @include('backoffice.content.items.message')

                        <h2>{{ $Type }} <small>Display All {{ $Type }}.</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                            @include('backoffice.content.items.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection

@section('extra-js')

    <!-- iCheck -->
    <script src="/backoffice/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="/backoffice/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/backoffice/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/backoffice/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/backoffice/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/backoffice/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/backoffice/vendors/pdfmake/build/vfs_fonts.js"></script>

@endsection