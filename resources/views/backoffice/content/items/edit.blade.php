@extends('backoffice.backoffice')

@section('title', 'Laravel Test | Edit '.$Type)

@section('extra-css')
    <!-- jquery Image Uplaod -->
    <link href="/backoffice/vendors/jquery-upload-preview/dist/css/bootstrap-imageupload.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="/backoffice/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="/backoffice/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="/backoffice/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="/backoffice/vendors/starrr/dist/starrr.css" rel="stylesheet">
@endsection

@section('content')
        <!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('backoffice') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li><a href="{{ route(strtolower($Type).'.index') }}">{{$Type}}</a></li>
                    <li class="active">Edit </li>
                </ol>

            </div>
        </div>
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{$Type}} <small>Edit</small></h2>

                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {!! Form::model($item, [
                             'method' => 'PUT',
                             'class'=> 'form-horizontal form-label-left',
                             'route'  => [strtolower($Type).'.update',$item->id],
                             'files'  => TRUE,
                             'novalidate' =>'',
                             'id' => 'post-form'
                         ]) !!}

                        @include('backoffice.content.items.form')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection

@section('extra-js')
    <!-- validator -->
    <script src="/backoffice/vendors/validator/validator.js"></script>
    <!-- jquery Image Uplaod -->
    <script src="/backoffice/vendors/jquery-upload-preview/dist/js/bootstrap-imageupload.js"></script>
    <script>
        var $imageupload = $('.imageupload');
        $imageupload.imageupload({
            allowedFormats: [ "jpg", "jpeg", "png" ]
        });
    </script>
    <!-- iCheck -->
    <script src="/backoffice/vendors/iCheck/icheck.min.js"></script>
    <!-- Switchery -->
    <script src="/backoffice/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="/backoffice/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="/backoffice/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="/backoffice/vendors/autosize/dist/autosize.min.js"></script>
@endsection