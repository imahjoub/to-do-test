
    <div class="item form-group {{ $errors->has('titre') ? 'bad' : '' }}">
        {!! Form::label('Titre', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::text('titre', null, ['class' => 'form-control','placeholder' => 'titre','required'=> 'required',  'data-validate-length-range' => '6']) !!}
        </div>
        @if($errors->has('titre'))
             <div class="alert">{{ $errors->first('titre') }}</div>
        @endif
    </div>

    <div class="item form-group  {{ $errors->has('description') ? 'bad' : '' }}">
        {!! Form::label('description*', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::textarea('description', null, ['class' => 'form-control col-md-7 col-xs-12', 'required' => 'required']) !!}
        </div>
        @if($errors->has('description'))
            <div class="alert">{{ $errors->first('description') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('status') ? 'bad' : '' }}">
        {!! Form::label('Status*', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            Pending: {!! Form::radio('status', 'pending' ,true,['class' => 'flat']) !!}
            Done: {!! Form::radio('status', 'done',false, ['class' => 'flat']) !!}
        </div>
        @if($errors->has('status'))
            <div class="alert">{{ $errors->first('status') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('dateTime') ? 'bad' : '' }}">
        {!! Form::label('Date Time (2017-11-13 11:48:16)', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::text('dateTime', null, ['class' => 'form-control','placeholder' => 'Date Time','required'=> 'required',  'data-validate-length-range' => '6']) !!}
        </div>
        @if($errors->has('dateTime'))
            <div class="alert">{{ $errors->first('dateTime') }}</div>
        @endif
    </div>

    <div class="item form-group {{ $errors->has('list') ? 'has-error' : '' }}">
        {!! Form::label('list*', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::hidden('list_id', $list->id) !!}
            <p class="form-control-static">{{ $list->titre }}</p>
        </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-1">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>