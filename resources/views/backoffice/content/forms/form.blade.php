
    <div class="item form-group {{ $errors->has('titre') ? 'bad' : '' }}">
        {!! Form::label('Titre', null, ['class' => 'control-label col-md-1 col-sm-1 col-xs-12']) !!}
        <div class="col-md-7 col-sm-7 col-xs-12">
            {!! Form::text('titre', null, ['class' => 'form-control','placeholder' => 'titre','required'=> 'required',  'data-validate-length-range' => '6']) !!}
        </div>
        @if($errors->has('titre'))
             <div class="alert">{{ $errors->first('titre') }}</div>
        @endif
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-1">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>