
<table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>id</th>
        <th>Title</th>
        <th>Created At</th>
        <th style="width: 25%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php $currentUser = auth()->user(); ?>
    @foreach( $listes as $list)
        <tr>
            {!! Form::open(['method' => 'DELETE', 'route' => [  strtolower($Type).'.destroy', $list->id]]) !!}
                <td>#{{$list->id}} </td>
                <td>{{$list->titre}}</td>
                <td>{{$list->created_at}}</td>
                <td>
                    <a href="{{route('item.showitem' , $list->id)}}" class="btn btn-warning  btn-xs "><i class="fa fa-pencil"></i> Items </a>
                    <a href="{{route(strtolower($Type).'.edit' , $list->id)}}" class="btn btn-info btn-xs "><i class="fa fa-pencil"></i> Edit </a>
                    <button onclick="return confirm('Êtes-vous sûr ?');" type="submit" class="btn btn-danger btn-xs">
                        <i class="fa fa-trash-o"></i> Delete
                    </button>
                </td>
            {!! Form::close() !!}
        </tr>
    @endforeach
    </tbody>
</table>