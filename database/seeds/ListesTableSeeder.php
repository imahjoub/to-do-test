<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Liste;

class ListesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('PRAGMA foreign_keys = OFF');
        DB::Table('listes')->Truncate();

        $faker = Faker\Factory::create();

        $user1 = User::find(1);
        $list = new Liste();
        $list->titre = 'Laravel course';
        $user1->listes()->save($list);

        $list = new Liste();
        $list->titre = 'CakePhp course';
        $user1->listes()->save($list);

        $list = new Liste();
        $list->titre = 'Wordpress course';
        $user1->listes()->save($list);

        $user2 = User::find(2);
        $list = new Liste();
        $list->titre = 'Symfony course';
        $user2->listes()->save($list);

        $list = new Liste();
        $list->titre = 'Magento course';
        $user2->listes()->save($list);

    }
}
