<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('PRAGMA foreign_keys = OFF');
        DB::Table('users')->Truncate();
        DB::Table('users')->insert([
            [
                'name' => 'User1',
                'email' => 'user1@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => '2017-07-19 08:00:00',
                'updated_at' => '2017-07-20 08:00:00',
            ],
            [
                'name' => 'User2',
                'email' => 'user2@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => '2017-07-19 08:00:00',
                'updated_at' => '2017-07-20 08:00:00',
            ]
        ]);
    }
}
