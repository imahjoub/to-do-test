<?php

use Illuminate\Database\Seeder;
use App\Liste;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('PRAGMA foreign_keys = OFF');
        DB::Table('items')->Truncate();

        $faker = Faker\Factory::create();

        $list = Liste::find(1);
        $item = new Item();
        $item->titre = 'Tache 1';
        $item->description = $faker->text(400);
        $item->status = 'done';
        $item->dateTime = '2017-11-13 11:48:16';
        $list->items()->save($item);

        $list = Liste::find(2);
        $item = new Item();
        $item->titre = 'Tache 2';
        $item->description = $faker->text(400);
        $item->status = 'pending';
        $item->dateTime = '2017-11-13 11:50:16';
        $list->items()->save($item);
    }
}
