<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', 'ListeController@index')->name('backoffice');

Route::resource('/dashboard/listes','ListeController');
Route::resource('/dashboard/items','ItemController');

Route::get('dashboard/listes/{id}/items/',[
    'uses' => 'ItemController@itemsBasedOnListId',
    'as' => 'item.showitem'
]);

Route::get('dashboard/listes/{id}/items/create',[
    'uses' => 'ItemController@create',
    'as' => 'listitem.create'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
