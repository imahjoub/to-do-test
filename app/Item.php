<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'titre',''
    ];
    /**
     * Get the List.
     */
    public function liste()
    {
        return $this->belongsTo('App\Liste');
    }
}
