<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liste extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre','description','status','dateTime'
    ];

    /**
     * Get the user that owns the list.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Items.
     */
    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
