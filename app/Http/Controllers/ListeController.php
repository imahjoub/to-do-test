<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Liste;
use App\Http\Requests;


class ListeController extends Controller
{

    private $Type;

    public function __construct()
    {
        $this->Type = "Listes";
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user      = Auth::user();
        $listes = $user->listes()->get();
        $Type = $this->Type;
        return view("backoffice.content.forms.index", compact('listes','Type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list = new Liste();
        $Type = $this->Type;
        return view("backoffice.content.forms.create", compact('list','Type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ListStoreRequest $request)
    {
        $user      = Auth::user();
        $list = new Liste();
        $list->titre = $request->get('titre');
        $user->listes()->save($list);

        return redirect(route('listes.index'))->with("success", "Nouvelle List Crée");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = Liste::findOrFail($id);
        $Type = $this->Type;
        return view("backoffice.content.forms.edit", compact('list','Type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ListUpdateRequest $request, $id)
    {

        $list = Liste::findOrFail($id);
        $list->update($request->all());

        return redirect(route('listes.index'))->with("success", "La Liste a été mise à jour !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = Liste::findOrFail($id);
        $list->delete();

        return redirect(route('listes.index'))->with("success", "La Liste a été supprimé !");
    }
}
