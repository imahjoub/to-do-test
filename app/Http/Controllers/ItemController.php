<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Liste;
use App\Item;
use App\Http\Requests;

class ItemController extends Controller
{
    private $Type;

    public function __construct()
    {
        $this->Type = "Items";
        $this->middleware('auth');
    }

    public function itemsBasedOnListId($id){
        $list = Liste::find($id);
        $items = $list->items()->get();
        $Type = $this->Type;
        return view("backoffice.content.items.index", compact('items','Type','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $item = new Item();
        $list = Liste::find($id);
        $Type = $this->Type;
        return view("backoffice.content.items.create", compact('item','Type','list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ItemStoreRequest $request)
    {
        $list_id = $request->get('list_id');
        $list = Liste::find($list_id);
        $item = new Item();
        $item->titre = $request->get('titre');
        $item->description = $request->get('description');
        $item->status = $request->get('status');
        $item->dateTime = $request->get('dateTime');
        $list->items()->save($item);

        return redirect(route('item.showitem',$list_id))->with("success", "Nouvel Item Crée");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        $list = Liste::find($id);
        $Type = $this->Type;

        return view("backoffice.content.items.edit", compact('list','Type','item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\ItemUpdateRequest $request, $id)
    {
        $list_id = $request->get('list_id');
        $item = Item::findOrFail($id);
        $item->update($request->all());

        return redirect(route('item.showitem',$list_id))->with("success", "L'item a été mise à jour !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        return redirect()->back()->with("success", "L'item a été supprimé !");
    }
}
